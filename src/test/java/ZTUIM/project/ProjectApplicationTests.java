package ZTUIM.project;

import ZTUIM.project.model.Student;
import ZTUIM.project.model.StudentDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ProjectApplicationTests {

	@Autowired
	private StudentDao studentDao;

	@Test
	void addStudentTest() {

		Student student = new Student();

		student.setFirstName("Sylwek");
		student.setLastName("Mikolajczuk");
		student.setEmail("sm@gmail.com");
		student.setDateOfBirth("02.04.2001");
		studentDao.save(student);
	}

}
